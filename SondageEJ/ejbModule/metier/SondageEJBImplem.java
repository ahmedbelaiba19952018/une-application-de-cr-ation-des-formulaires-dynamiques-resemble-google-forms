package metier;

import java.util.List;

import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import metier.entities.Question;
import metier.entities.Sondage;
import metier.entities.proposition;
@Stateless(name="BK")
public class SondageEJBImplem implements SondageRemote,SondageLocale {
	@PersistenceContext
    private EntityManager em;
	@Override
	public void addSondage(Sondage sondage) {
		em.persist(sondage);
		
	}

	@Override
	public void addQuestion(Question question, Long idsondage) {
		Sondage sond=em.find(Sondage.class, idsondage);
		question.setSondage(sond);
		em.persist(question);
		
	}

	@Override
	public void addProposition(proposition proposition, Long idquestion) {
		Question quest=em.find(Question.class, idquestion);
		proposition.setQuestion(quest);
		em.persist(proposition);
		
	}

	@Override
	public Sondage consulterSondage(Long id_sond) {
		Sondage sondage=em.find(Sondage.class,id_sond);
		if(sondage==null) throw new RuntimeException("sondage"+id_sond+"n'existe pas");
		return sondage;
	}

	@Override
	public List<Question> consulterQuestionparsondage(Long id_sond) {
		javax.persistence.Query req=em.createQuery("select q from Question q where q.sondage.id_sonda=:x");
		req.setParameter("x", id_sond);
		return req.getResultList();
	}

	@Override
	public List<proposition> consulterpropositionparquestion(Long is_question) {
		javax.persistence.Query req=em.createQuery("select p from proposition p where p.question.id_question=:x");
		req.setParameter("x", is_question);
		return req.getResultList();
	}

}
