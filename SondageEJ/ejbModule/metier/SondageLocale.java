package metier;



import java.util.List;

import javax.ejb.Local;

import metier.entities.Question;
import metier.entities.Sondage;
import metier.entities.proposition;

@Local
public interface SondageLocale {
	public void addSondage(Sondage sondage);
	public void addQuestion(Question question,Long idsondage);
	public void addProposition(proposition proposition,Long idquestion);
	public Sondage consulterSondage(Long id_sond);
	public List<Question> consulterQuestionparsondage(Long id_sond);
	public List<proposition> consulterpropositionparquestion(Long is_question);

}
