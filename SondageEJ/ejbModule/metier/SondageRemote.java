package metier;

import javax.ejb.Remote;

import metier.entities.Question;
import metier.entities.Sondage;
import metier.entities.proposition;

@Remote
public interface SondageRemote {
	public void addSondage(Sondage sondage);
	public void addQuestion(Question question,Long idsondage);
	public void addProposition(proposition proposition,Long idquestion);
}
