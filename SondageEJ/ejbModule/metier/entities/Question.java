package metier.entities;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Question
 *
 */
@Entity

public class Question implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_question;
	private String intitule;
	private String type;
	@OneToMany(mappedBy="question",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private Collection<proposition> propositions;
	@ManyToOne
	@JoinColumn(name="id_sondage")
	private Sondage sondage;
	
	private static final long serialVersionUID = 1L;
    
	public Question(String intitule, String type) {
		super();
		this.intitule = intitule;
		this.type = type;
	}
	public Sondage getSondage() {
		return sondage;
	}
	public void setSondage(Sondage sondage) {
		this.sondage = sondage;
	}
	public Question() {
		super();
	}   
	public Long getId_question() {
		return this.id_question;
	}

	public void setId_question(Long id_question) {
		this.id_question = id_question;
	}   
	public String getIntitule() {
		return this.intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public Collection<proposition> getPropositions() {
		return propositions;
	}
	public void setPropositions(Collection<proposition> propositions) {
		this.propositions = propositions;
	}
   
}
