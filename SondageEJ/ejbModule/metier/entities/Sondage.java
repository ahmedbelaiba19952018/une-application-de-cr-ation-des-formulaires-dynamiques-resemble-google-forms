package metier.entities;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Sondage
 *
 */
@Entity

public class Sondage implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_sonda;
	private String intitule;
	@OneToMany(mappedBy="sondage",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private Collection<Question> questions;
	private static final long serialVersionUID = 1L;

	public Sondage() {
		super();
	} 
	
	public Sondage(String intitule) {
		super();
		this.intitule = intitule;
	}

	public Long getId_sonda() {
		return this.id_sonda;
	}

	public void setId_sonda(Long id_sonda) {
		this.id_sonda = id_sonda;
	}   
	public String getIntitule() {
		return this.intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
   
}
