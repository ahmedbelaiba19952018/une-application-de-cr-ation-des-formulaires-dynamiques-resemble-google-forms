package metier.entities;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: proposition
 *
 */
@Entity

public class proposition implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_propo;
	private String intitule;
	@ManyToOne
	@JoinColumn(name="id_question")
	private Question question;
	private static final long serialVersionUID = 1L;
    
	public proposition(String intitule) {
		super();
		this.intitule = intitule;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public proposition() {
		super();
	}   
	public Long getId_propo() {
		return this.id_propo;
	}

	public void setId_propo(Long id_propo) {
		this.id_propo = id_propo;
	}   
	public String getIntitule() {
		return this.intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
   
}
