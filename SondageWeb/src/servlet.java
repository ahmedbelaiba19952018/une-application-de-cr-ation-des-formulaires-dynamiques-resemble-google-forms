import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.PRIVATE_MEMBER;

import metier.SondageLocale;
import metier.entities.Question;
import metier.entities.Sondage;
import metier.entities.proposition;

public class servlet extends HttpServlet
{

	/**
	 * 
	 */
	@EJB
	private SondageLocale metier;
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException 
	{
		/*PrintWriter out=resp.getWriter();
		out.println("ahmed");*/
		PrintWriter out=resp.getWriter();
		String intitule=req.getParameter("titre");
		Sondage sondage=new Sondage(intitule);
	    metier.addSondage(sondage);
		Enumeration<String> parameterNames=req.getParameterNames();
		while(parameterNames.hasMoreElements())
		{
			String paramName=parameterNames.nextElement();
			System.out.println(paramName);
			if(!paramName.equals("titre")) {
			String[] paramvalues=req.getParameterValues(paramName);
			String paramvalue=paramvalues[0];
			String paramvaluee=paramvalues[paramvalues.length-1];
			Question question=new Question(paramvalue, paramvaluee);
			metier.addQuestion(question, sondage.getId_sonda());
			for(int i=1;i<paramvalues.length-1;i++)
			{
				String paramvalueee=paramvalues[i];
				proposition prop=new proposition(paramvalueee);
				metier.addProposition(prop, question.getId_question());
				System.out.println("\t"+paramvalueee);
			}
			}
		}
		out.println("<h3>votre sondage intitul� \""+intitule+"\" a �t� bien cr�e</h3><br/> ");
		out.println("<a href=\"controleur?id="+sondage.getId_sonda()+"\">aper�u</a>"+"&nbsp;&nbsp;&nbsp;");
		out.println("<a href=\"test.html\">cr�er un autre sondage</a>");
		

	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out=resp.getWriter();
		Long id_sondage=Long.parseLong(req.getParameter("id"));
		out.println("<h1>"+metier.consulterSondage(id_sondage).getIntitule()+"</h1>");
		List<Question> liste_question=metier.consulterQuestionparsondage(id_sondage);
		 for(int i=0; i<liste_question.size(); i++) 
		 {
			   out.println("<h2>question"+(i+1)+"</h2>");
	           
			   out.print("<h3>"+liste_question.get(i).getIntitule()+"</h3>");
			  
	           out.println("<ul>"); 
	            List<proposition> listeproposition=metier.consulterpropositionparquestion(liste_question.get(i).getId_question());
	            out.println("<table>");
            	if(liste_question.get(i).getType().equals("Choix multiples"))
            	{
	            for(int j=0;j<listeproposition.size();j++)
	            {
	            	out.println("<tr>");
	            	out.println("<td><li>"+listeproposition.get(j).getIntitule()+"</td><td><INPUT type=\"checkbox\"></li></td>");
	            	out.println("</tr>");
	            }
	            out.println("</table>");
	            out.println("</ul>");
	            
            	}else
            	{
            		
            		
            		 for(int j=0;j<listeproposition.size();j++)
     	            {
            			 out.println("<tr>");
     	            	out.println("<td><li>"+listeproposition.get(j).getIntitule()+"</td><td><INPUT type=\"radio\" name=\"contact\"></li></td>");
     	            	out.println("</tr>");
     	            }
            		 out.println("</table>");
     	            out.println("</ul>");
     	           
            		
            	}
		 }
		 out.println("<input type=\"submit\" value=\"envoyer\"/>");
		 out.println("<a href=\"test.html\">cr�er un autre sondage</a>");
	}
	

}
